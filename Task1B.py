from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

Stations = build_station_list()
p = (52.2053, 0.1218)
x = stations_by_distance(Stations, p)

print(x[:10])
print(x[-10:])
