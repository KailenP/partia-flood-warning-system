from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

Stations = build_station_list()
p = (50, 0)
x = stations_by_distance(Stations, p)

def test_distance():
    for a, b in x:
        assert b>0
