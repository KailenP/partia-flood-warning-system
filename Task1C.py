from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

stations = build_station_list()
x = (52.2053, 0.1218)

m = stations_within_radius(stations, x, 10)
m.sort()
print(m)


