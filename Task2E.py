import matplotlib
import matplotlib.pyplot as plt
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list
import datetime
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels


stations = build_station_list()
dt = 10 
update_water_levels(stations)
risk = stations_highest_rel_level(stations,5)
for k in risk:
    dates, levels = fetch_measure_levels(k.measure_id, dt=datetime.timedelta(days=dt))
    plot_water_levels(k, dates, levels)
   
    
