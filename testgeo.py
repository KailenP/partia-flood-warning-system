from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number
stations=build_station_list()
#test for task 1D
def test_rivers_with_station():
    x=rivers_with_station(stations)
    assert len(x)>0
def test_stations_by_river():
    y=stations_by_river(stations)
    assert len(y)>0
#test for task 1E
def test_rivers_by_station_number():
    n=10
    z=rivers_by_station_number(stations,n)
    assert len(z)>0

test_rivers_with_station()
test_stations_by_river()
test_rivers_by_station_number()