from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

stations = build_station_list()
centre = (50,0)

def test_radius():
    x = stations_within_radius(stations, centre, 5)
    for k in x:
        assert type(k) == str
        
