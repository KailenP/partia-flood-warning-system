from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

stations=build_station_list(use_cache=True)
a=list(rivers_with_station(stations))
b=len(a)
print(b)
a.sort()
print(a[:10])
print(stations_by_river(stations)['River Aire'])
print(stations_by_river(stations)['River Cam'])
print(stations_by_river(stations)['River Thames'])
if __name__ == "__main__":
    print("Task1D")