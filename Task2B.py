from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold
tol=0.8
stations=build_station_list()
update_water_levels(stations)
x=stations_level_over_threshold(stations, tol) #returns tuple of station and relative
for k in x:
    kailen = k[0]
    print (kailen.name, k[1])



if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")