from floodsystem.analysis import polyfit
from floodsystem.stationdata import build_station_list
import matplotlib
import matplotlib.pyplot as plt 
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime
from floodsystem.flood import stations_highest_rel_level
import numpy as np
from floodsystem.stationdata import update_water_levels

stations = build_station_list()
dt = 2
p = 4
update_water_levels(stations)
risk_stations = stations_highest_rel_level(stations, 5)
for station in risk_stations:
    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
    plot_water_level_with_fit(station, dates, levels, p)
    
    




