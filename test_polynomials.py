from floodsystem.analysis import polyfit
from floodsystem.stationdata import build_station_list
import matplotlib
import matplotlib.pyplot as plt 
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime
from floodsystem.flood import stations_highest_rel_level
import numpy as np
from floodsystem.stationdata import update_water_levels

#this test checks the mean value of the polynomial fit is within 10% of the actual levels

stations = build_station_list()
dt = 5
update_water_levels(stations)
risky = stations_highest_rel_level(stations,5)

def test_polynomialfit():
    for station in risky:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        kailen = polyfit(dates, levels, 4)
        y, z = kailen
        numdates = matplotlib.dates.date2num(dates)
        h = np.linspace(numdates[0], numdates[-1], 20)
        polynomial_values = y(h-z)
        mean_actual = sum(levels)/len(levels)
        mean_poly = sum(polynomial_values)/len(polynomial_values)
        error = (mean_actual -mean_poly)/mean_actual
        assert abs(error) < 0.1




