from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels
import datetime

#This test makes sure that for every dates reading it has a corresponding levels reading 
#also checks that there are no duplicate dates readings
stations = build_station_list()
dt = 5
update_water_levels(stations)
risky = stations_highest_rel_level(stations,5)


def test_plotting():
    for station in risky:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        assert len(dates) == len(levels)
        assert len(dates) == len(set(dates))

