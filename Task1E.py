from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

stations=build_station_list(use_cache=True)
print(rivers_by_station_number(stations,N=9))
