from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
import numpy as np

def test_task2b():
    stations = build_station_list()
    update_water_levels(stations)

    tol = 0.8

    flood_risk_list = stations_level_over_threshold(stations, tol)

    for i in flood_risk_list:
        assert i[1] > tol

def test_task2c():
    stations = build_station_list()
    update_water_levels(stations)

    flood_risk_list = stations_highest_rel_level(stations, 10)
    
    for i in range(len(flood_risk_list)-1):
        assert flood_risk_list[i][1] >= flood_risk_list[i+1][1]