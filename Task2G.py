from floodsystem.stationdata import build_station_list
from floodsystem.dangersort import severity_sorter
from floodsystem.stationdata import update_water_levels

stations = build_station_list()
update_water_levels(stations)

severity_sorter(stations)