from floodsystem.stationdata import build_station_list
from floodsystem.analysis import gradient
from floodsystem.datafetcher import fetch_measure_levels
import datetime


dt=2
stations = build_station_list()
station = stations[0]

dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))

p = type(dates[2])
print(p)
k = len(dates)
print (k)

f=gradient(dates, levels)
print(f)

for station in high_severe:
    dt = 2
    try:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
    except: 
        print("Warning {} in {} has unavailable data".format(station.name, station.town))
        severe.append(station.town)
    if len(dates) <3 :
        continue
    k = gradient(dates, levels)
    t,d = station.typical_range
    grad_limit = (d-t)/2 #if the gradient becomes more than half the typical range in a day, limit set
    if k<grad_limit:
        high.append(station.town)
    elif k> grad_limit:
        severe.append(station.town)
