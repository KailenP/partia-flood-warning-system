from floodsystem.stationdata import build_station_list
from floodsystem.analysis import gradient
from floodsystem.analysis import polyfit
from floodsystem.geo import nearest_station
from floodsystem.datafetcher import fetch_measure_levels
import datetime
from floodsystem.stationdata import update_water_levels
import matplotlib


def severity_sorter(stations):
    low =[]
    moderate =[]
    high = []
    severe =[]
    high_severe =[] #list of both high and severe STATION OBJECTS which will be sub sorted by water rise rate
    unavailable_range =[] #list of station objects with unavailable relative danger, need to use nearest town


    for station in stations:
        if station.relative_water_level() == None:
            unavailable_range.append(station) 
        elif station.relative_water_level() <0.5:
            low.append(station.town)
        elif station.relative_water_level() <0.8:
            moderate.append(station.town)
        elif station.relative_water_level() <1.5:
            high.append(station.town)
        else:
            severe.append(station.town)

    for station in unavailable_range:
        neighbour = nearest_station(stations, station.coord) #returns neighbouring station for unavailable
        if neighbour.town != station.town:
            if neighbour.town in severe:
                severe.append(station.town)
            elif neighbour.town in high:
                high.append(station.town)
            elif neighbour.town in moderate:
                moderate.append(station.town)
            elif neighbour.town in low:
                low.append(station.town)


    #delete duplicate entries of towns from lower severity lists,
    for x in high:
        if x in severe:
            high.remove(x)

    for y in moderate:
        if y in high:
            moderate.remove(y)

    for z in low:
        if z in moderate:
            low.remove(z) 

    print("no. low: {}".format(len(low)))
    print("no. moderate: {}".format(len(moderate)))
    print("no. high: {}".format(len(high)))
    print("no. severe: {}".format(len(severe)))

    print('low risk: {} \n\n\n, moderate risk: {} \n\n\n, high risk: {} \n\n\n, severe risk: {}'.format(low, moderate, high, severe))

