#dates is a list of the date of measurements, change to float first
#levels is a reading of wwater level
#p is x axis shift
import numpy as np
import matplotlib.dates


def polyfit (dates, levels, p):
    r = matplotlib.dates.date2num(dates) #change datetime objects to floats
    p_coeff = np.polyfit(r - r[0], levels, p)
    dtheta = r[0]
    poly = np.poly1d(p_coeff)
    return poly, dtheta


#task 2G

def gradient(dates, levels): #gives the gradient in water level per day
    p = polyfit(dates, levels, 4)
    a, correction = p
    derivative = np.polyder(a)
    numdates = matplotlib.dates.date2num(dates)
    r = derivative(numdates[-1] - correction) #finds the gradient at the final reading, need to minus the correction term to allow the polynomial to work
    return r


