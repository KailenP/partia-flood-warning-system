# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from floodsystem.stationdata import build_station_list

from floodsystem.utils import sorted_by_key

from haversine import haversine, Unit

#Task 1B   
def stations_by_distance(Stations, p):
    l = []
    for k in Stations:
        info = ([k.name, k.town], haversine(k.coord, p))
        l.append(info)
    return sorted_by_key(l, 1)


#Task 2G
def nearest_station(Stations, p):
    l = []
    for k in Stations:
        info = (k, haversine(k.coord, p))
        l.append(info)
    m = sorted_by_key(l, 1) #tuple list of station object with distance from p, sorted by distance
    r = m[0] #nearest station, distance tuple
    return r[0] #pull station object only


#Task 1C
def stations_within_radius(stations, centre, r):
    selected = []
    for k in stations:
        if haversine(k.coord, centre) < r:
            selected.append(k.name)
    return selected

#Task 1D part 1
def rivers_with_station(stations):
    number=len(stations)
    rivers=[None]*number
    for i in range(0,number):
        rivers[i]=stations[i].river
    happy_oscar=set(rivers)
    return happy_oscar
#Task 1D part 2
def stations_by_river(stations):
    dict={}
    for station in stations:
        if station.river not in dict:
            dict[station.river]=[station.name]
        else:
            dict[station.river].append(station.name)
    return dict

#Task 1E
def rivers_by_station_number(stations, N):
    stations_number = {}
    for station in stations:
        if station.river not in stations_number:
            stations_number[station.river] = 1
        else:
            stations_number[station.river] += 1
    
    #define a function to take the second value.
    def Number(x):
        return x[1]
    # sort list based on number of stations
    river_count = []
    
    for i in stations_number:
        river_count.append((i, stations_number[i]))
    river_sorted=river_count
    river_sorted.sort(key=Number,reverse=True)
     
    
    # add N rivers to new list
    n_river = []
    for i in range(N):
        n_river.append(river_sorted[i])

    # add rivers with same number of stations after the Nth entry
    counter = N - 1
    while river_sorted[counter][1] == river_sorted[counter + 1][1]:
        n_river.append(river_sorted[counter + 1])
        counter += 1
    return n_river



