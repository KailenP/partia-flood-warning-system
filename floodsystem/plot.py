import matplotlib
import matplotlib.pyplot as plt
import matplotlib.axes
from floodsystem.analysis import polyfit
import numpy as np

def plot_water_levels(station, dates, levels): #task 1E
    plt.plot(dates, levels)
    plt.title('{}'.format(station.name))
    plt.xlabel('date')
    plt.ylabel('water level')
    plt.xticks(rotation = 45)
    plt.tight_layout
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    dragon = polyfit(dates, levels, p) #returns polynomial function fit and x shift
    polynomial, correction = dragon
    numdates = matplotlib.dates.date2num(dates)
    h = np.linspace(numdates[0], numdates [-1], 40) #forms x values to tabulate with polynomial
    plt.plot(h, polynomial(h - correction)) #plot recorrected polynomial (shift x axis to the right again by correction)
    plot_water_levels(station, dates, levels) #overlay actual data
    lower, upper = station.typical_range
    plt.axhline(y=lower)
    plt.axhline (y=upper)
    plt.xticks(rotation=45)
    plt.tight_layout
    

    
    
